# Inventory Pattern Library

B Team base pattern library made with Gulp and SCSS.

### Setup

1. Running with Node 14, so install that using `nvm install 14`
2. Reinstall Gulp `npm install gulp -g`
3. Helpful to add a file called `.nvmrc` to the root with the number you want to use with Node (14)

### Install

1. Change directory into the root folder `patterns`
2. `npm install`
3. `gulp serve` to get going

### Build only

Run `gulp build` to just process everything into the `public` folder.

This also inclused UnCSS, which checks the `public` folder HTML files for classes and then strips out unused ones from the CSS.

### Clean up CSS

Run `gulp clean` to do a clean up with UNCSS, which checks the `public` folder HTML files for classes and then strips out unused ones from the CSS.

### Icons

`gulp icons:build` builds the icon sprite from SVG icons placed in the `assets/icons/source` folder.

Reference in your markup as follows (where icon is exampleicon)

```
<svg class="icon_exampleicon_svg">
  <use xlink:href="/assets/icons/renders/sprite.svg#exampleicon"></use>
</svg>
```

### Deployment

Uses surge.sh

1. Open `cname/CNAME`
2. Add your surge URL e.g. `my-project.surge.sh`
3. Deploy from the root with command `surge public` or from `public` folder with command `surge`

### Credits

Based on an early iteration of Abernathy, a framework created by Pete Coles - https://github.com/petercolesdc/abernathy. 
Updated with work by Russell Kirkland and Ben Darby as part of B Team projects. 
