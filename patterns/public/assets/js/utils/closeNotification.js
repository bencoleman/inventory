// Allow iframes in content to be wrapped in responsive code
import $ from 'jquery';

export default function closeNotification (
		alertDismissedCookiePrefix = "cw-alert-dismissed--",
        alertDismissedDays = -1,
        cookieAttributes = {
            path: '/',
        }
    )
{

    $('.alert__dismiss').on('click', function(event) {
        event.preventDefault();

        $(this).parents('.alert').slideUp(400, function() {
            $(this).remove();

            // set a cookie
            var cookieName = alertDismissedCookiePrefix + $(this).attr('id'),
                cookieExpiresDays = parseInt($(this).data('dismissed-for-days'), 10);

            if (cookieExpiresDays > 0) {
                cookieAttributes.expires = parseInt(cookieExpiresDays, 10);
            }

            Cookies.set(cookieName, 1, cookieAttributes);
        });
    });

    $.root.find('.alert').each(function() {
        var cookieName = alertDismissedCookiePrefix + $(this).attr('id');

        if (!Cookies.get(cookieName)) {
            $(this).removeClass('alert--pre-initialized');
        }
    });
}